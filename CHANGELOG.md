# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.7](https://gitlab.com/renanhangai_/nodejs/scrappy/compare/v0.0.6...v0.0.7) (2020-01-19)

### [0.0.6](https://gitlab.com/renanhangai_/nodejs/scrappy/compare/v0.0.5...v0.0.6) (2020-01-19)

### [0.0.5](https://gitlab.com/renanhangai_/nodejs/scrappy/compare/v0.0.4...v0.0.5) (2020-01-14)

### [0.0.4](https://gitlab.com/renanhangai_/nodejs/scrappy/compare/v0.0.3...v0.0.4) (2020-01-14)

### [0.0.3](https://gitlab.com/renanhangai_/nodejs/scrappy/compare/v0.0.2...v0.0.3) (2020-01-14)


### Bug Fixes

* Error when performing request ([87f50ea](https://gitlab.com/renanhangai_/nodejs/scrappy/commit/87f50ea5b8739df4f12a3bcf4a855a6a38f7e5dc))

### [0.0.2](https://gitlab.com/renanhangai_/nodejs/scrappy/compare/v0.0.1...v0.0.2) (2020-01-14)


### Bug Fixes

* Added build dependencies ([6865505](https://gitlab.com/renanhangai_/nodejs/scrappy/commit/6865505dbd891c98b478503ae33cdc2e1b4e1d08))

### 0.0.1 (2020-01-14)


### Features

* Added HTTPS certificate support ([ebfa3b2](https://gitlab.com/renanhangai_/nodejs/scrappy/commit/ebfa3b228a96a15cac083fcdb22dde217e6a1338))
* Added post form data ([587dd92](https://gitlab.com/renanhangai_/nodejs/scrappy/commit/587dd928b05430888260bf0c7b6f75af0a464593))
* Added request with retries ([a911283](https://gitlab.com/renanhangai_/nodejs/scrappy/commit/a911283a26c77be8485684afb00f8b4ded095440))
