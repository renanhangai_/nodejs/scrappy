import { WebScrapper, WebScrapperRequestOptions, WebScrapperResponse } from './WebScrapper';
import request from 'request-promise-native';
import { CookieJar, Response } from 'request';

/**
 *
 */
export class WebScrapperRequest implements WebScrapper {
	private pending: Set<any> = new Set();
	private cookieJar: CookieJar;
	private requestInstance: request.RequestPromiseAPI;

	constructor() {
		this.cookieJar = request.jar();
		this.requestInstance = request.defaults({ jar: this.cookieJar });
	}
	/**
	 * Clear every pending request
	 */
	async clear() {
		this.pending.forEach(request => {
			if (request.abort) request.abort();
		});
		this.pending.clear();
	}
	/**
	 * Do the request
	 */
	async request(options: WebScrapperRequestOptions): Promise<WebScrapperResponse> {
		const rawResponse = this.requestInstance({
			method: options.method,
			url: options.url,
			headers: options.headers,
			body: options.body,
			resolveWithFullResponse: true,
			simple: options.simple !== false,
			encoding: null,
			agentOptions: options.agentOptions,
		});
		this.pending.add(rawResponse);
		try {
			const response: Response = await rawResponse.promise();
			return {
				statusCode: response.statusCode,
				headers: response.headers,
				body: response.body,
			};
		} finally {
			this.pending.delete(rawResponse);
		}
	}
}
