import { ScrappyHeaders, ScrappyRequestMethod } from '../Types';
import https from 'https';

export type WebScrapperRequestOptions = {
	url: string;
	method: ScrappyRequestMethod;
	headers: ScrappyHeaders;
	body: null | Buffer | string;
	agentOptions?: https.AgentOptions;
	simple: boolean;
};
export type WebScrapperResponse = {
	statusCode: number;
	headers: ScrappyHeaders;
	body: Buffer;
};

export interface WebScrapper {
	/**
	 * Perform the setup if necessary
	 */
	setup?(): any;
	/**
	 * Clear the pending requests
	 */
	clear(): any;
	/**
	 * Do the request
	 */
	request(options: WebScrapperRequestOptions): Promise<WebScrapperResponse>;
}
