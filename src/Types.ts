import https from 'https';

export type ScrappyHeaders = {
	[headerName: string]: string | Array<string>;
};
export type ScrappyRequestMethod = 'post' | 'get';
export enum ScrappyResponseType {
	JSON = 'json',
	HTML = 'html',
	BUFFER = 'buffer',
	TEXT = 'text',
}

/**
 * Request body options
 */
export type ScrappyRequestBodyOptions =
	| ScrappyRequestBodyOptionsForm
	| ScrappyRequestBodyOptionsJson
	| ScrappyRequestBodyOptionsRaw;
export type ScrappyRequestBodyOptionsForm = {
	form: {
		[key: string]: string | Array<string>;
	};
};
export type ScrappyRequestBodyOptionsJson = {
	json: any;
};
export type ScrappyRequestBodyOptionsRaw = {
	raw: Buffer | string;
};

/**
 * Request retry options
 */
export type ScrappyRequestRetryOptions = {
	/// Number of attempts
	attempts?: number;
	/// Test function to try after every request to check if it was successfull
	test?: (
		arg: { response: ScrappyResponse; attempt: number },
		bail: (err?: Error) => void
	) => boolean | Promise<boolean>;
	/// Called on every error
	onError?: (arg: { error: Error; attempt: number }, bail: (err?: Error) => void) => any;
	/// Called on every retry
	onRetry?: (arg: { attempt: number }, bail: (err?: Error) => void) => any;
};

/**
 * Request options
 */
export type ScrappyRequestOptions = {
	method?: ScrappyRequestMethod;
	url: string | URL;
	query?: { [key: string]: string } | string;
	headers?: ScrappyHeaders;
	body?: ScrappyRequestBodyOptions;
	responseType?: ScrappyResponseType;
	responseEncoding?: string;
	simple?: boolean;
	https?: {
		pfx: string | Buffer | Array<string | Buffer | Object>;
		certificate?: string | Buffer | Array<string | Buffer>;
		privateKey?: string | Buffer | Array<Buffer | Object>;
		passphrase?: string;
		agentOptions?: https.AgentOptions;
	};
	retry?: ScrappyRequestRetryOptions;
};

/**
 * Options to create the scrapper
 */
export type ScrappyOptions = {
	defaults?: ScrappyRequestOptions;
};

/**
 * Response type
 */
export type ScrappyResponseBody = {
	buffer: Buffer;
	text?: string;
	json?: any;
	$?: CheerioStatic;
};

export type ScrappyResponse = {
	statusCode: number;
	headers: ScrappyHeaders;
	body: ScrappyResponseBody;
};
