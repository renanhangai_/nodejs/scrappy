import { Scrappy } from '../Scrappy';

/**
 * Single step on scrappy
 */
export type ScrappyStepRunOptions = {
	state: any;
	bail: (err?: Error) => void;
};
/**
 * Single step on scrappy
 */
export type ScrappyStep<R = any> = {
	order?: number;
	allowErrors?: boolean;
	run: (scrappy: Scrappy, options: ScrappyStepRunOptions) => R | Promise<R>;
};
/// Infer the result type using the step
export type ScrappyStepResult<T> = T extends ScrappyStep<infer R> ? Readonly<R> : any;

/**
 * Map of steps
 */
export type ScrappySteps = {
	[key: string]: ScrappyStep<any>;
};

/**
 * Result of executing the steps.
 */
export type ScrappyStepsResult<Steps extends ScrappySteps = ScrappySteps> = {
	[K in keyof Steps]: ScrappyStepResult<Steps[K]>;
};

/**
 * Run the steps
 */
export class ScrappyStepRunner {
	/// Create the step runner
	constructor(private readonly scrappy: Scrappy) {}
	/**
	 *
	 * @param steps
	 */
	async run<Steps extends ScrappySteps>(steps: Steps): Promise<ScrappyStepsResult<Steps>> {
		const result: ScrappyStepsResult = {};
		const orderedSteps = this.getStepsOrdered(steps);

		let isBailed = false;
		let error: Error = null;
		const bailCallback = (err?: Error) => {
			error = err || null;
			isBailed = true;
		};

		for (const currentStep of orderedSteps) {
			let currentResult: any = null;
			try {
				currentResult = await currentStep.run(this.scrappy, {
					state: result,
					bail: bailCallback,
				});
			} catch (err) {
				if (!currentStep.allowErrors) {
					throw err;
				}
				currentResult = null;
			}
			if (isBailed) {
				error = error || new Error('Bailing request');
				throw error;
			}
			result[currentStep.key] = currentResult;
		}
		return result as ScrappyStepsResult<Steps>;
	}

	/**
	 *
	 * @param steps
	 */
	private getStepsOrdered(steps: ScrappySteps): Array<ScrappyStep & { key: string }> {
		const stepsArray: Array<ScrappyStep & { key: string }> = [];
		for (const key in steps) {
			stepsArray.push({
				...steps[key],
				key,
			});
		}
		stepsArray.sort((a, b) => {
			const orderA = a.order | 0;
			const orderB = b.order | 0;
			return orderA - orderB;
		});
		return stepsArray;
	}
}
