import {
	ScrappyRequestBodyOptionsForm,
	ScrappyRequestBodyOptionsJson,
	ScrappyRequestBodyOptions,
	ScrappyRequestBodyOptionsRaw,
	ScrappyHeaders,
} from './Types';
import qs from 'querystring';

export type ScrappyBodySerialized = {
	body: Buffer | string | null;
	headers: ScrappyHeaders | null;
};

export class ScrappyBody {
	/**
	 * Serialize the input according to its type
	 * @param inputBody
	 */
	static serialize(inputBody: ScrappyRequestBodyOptions): ScrappyBodySerialized {
		let body: Buffer | string | null = null;
		let headers: ScrappyHeaders = null;
		if (ScrappyBody.isRaw(inputBody)) {
			body = inputBody.raw;
		} else if (ScrappyBody.isForm(inputBody)) {
			headers = { 'content-type': 'application/x-www-form-urlencoded' };
			body = qs.stringify(inputBody.form);
		} else if (ScrappyBody.isJson(inputBody)) {
			headers = { 'content-type': 'application/json' };
			body = JSON.stringify(inputBody.json);
		}
		return { body: body, headers };
	}
	static isForm(body: ScrappyRequestBodyOptions): body is ScrappyRequestBodyOptionsForm {
		return !!(body as ScrappyRequestBodyOptionsForm).form;
	}
	static isJson(body: ScrappyRequestBodyOptions): body is ScrappyRequestBodyOptionsJson {
		return !!(body as ScrappyRequestBodyOptionsJson).json;
	}
	static isRaw(body: ScrappyRequestBodyOptions): body is ScrappyRequestBodyOptionsRaw {
		return !!(body as ScrappyRequestBodyOptionsRaw).raw;
	}
}
