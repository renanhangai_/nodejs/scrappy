import {
	WebScrapper,
	WebScrapperRequestOptions,
	WebScrapperResponse,
} from './scrapper/WebScrapper';
import { WebScrapperRequest } from './scrapper/WebScrapperRequest';
import {
	ScrappyRequestRetryOptions,
	ScrappyRequestOptions,
	ScrappyResponse,
	ScrappyResponseType,
	ScrappyOptions,
	ScrappyHeaders,
	ScrappyRequestBodyOptionsForm,
	ScrappyRequestBodyOptionsJson,
	ScrappyResponseBody,
} from './Types';
import { URL, URLSearchParams } from 'url';
import { AgentOptions } from 'https';
import cheerio from 'cheerio';
import { ScrappyStepRunner, ScrappySteps, ScrappyStepsResult } from './runner/StepRunner';
import { ScrappyBody } from './ScrappyBody';

/**
 * Request options
 */
type ScrappyRequestNormalizedRetryOptions = Omit<ScrappyRequestRetryOptions, 'attempts'> & {
	attempts: number;
};
type ScrappyRequestNormalizedOptions = {
	retry: ScrappyRequestNormalizedRetryOptions;
	scrapperOptions: WebScrapperRequestOptions;
};

export class Scrappy {
	private scrapper: WebScrapper | null = null;
	private scrapperSetupPromise: Promise<void> | null = null;
	private readonly options: ScrappyOptions;

	/// Construct the scrapper
	constructor(options?: ScrappyOptions) {
		this.options = options || {};
	}

	/**
	 * Setup the request instance
	 */
	async setup() {
		if (this.scrapper) return;
		if (this.scrapperSetupPromise) {
			await this.scrapperSetupPromise;
			return;
		}
		try {
			this.scrapperSetupPromise = this.doSetup();
			await this.scrapperSetupPromise;
		} finally {
			this.scrapperSetupPromise = null;
		}
	}

	/**
	 * Setup the request instance
	 */
	private async doSetup() {
		const scrapper: WebScrapper = new WebScrapperRequest();
		if (scrapper.setup) {
			await scrapper.setup();
		} else {
			await new Promise(resolve => setImmediate(resolve));
		}
		this.scrapper = scrapper;
	}

	/**
	 * Clear the request
	 */
	async clear() {
		if (this.scrapper) {
			const scrapper = this.scrapper;
			this.scrapper = null;
			await scrapper.clear();
		}
	}

	/// Merge two options
	private async mergeRequestOptions(
		optionsA: ScrappyRequestOptions,
		optionsB: ScrappyRequestOptions
	): Promise<ScrappyRequestOptions> {
		if (!optionsA) return { ...optionsB };
		if (!optionsB) return { ...optionsA };
		return {
			method: optionsB.method || optionsA.method,
			url: optionsB.url || optionsA.url,
			query: optionsB.query || optionsA.query,
			body: optionsA.body || optionsB.body,
			headers: {
				...optionsA.headers,
				...optionsB.headers,
			},
			responseType: optionsB.responseType || optionsA.responseType,
			responseEncoding: optionsB.responseEncoding || optionsA.responseEncoding,
			simple: optionsA.simple || optionsB.simple,
			https: {
				...optionsA.https,
				...optionsB.https,
			},
			retry: {
				...optionsA.retry,
				...optionsB.retry,
			},
		};
	}

	/**
	 * Normaliza the options to pass to the scrapper implementation
	 * @param options
	 */
	private async normalizeRequestOptions(
		options: ScrappyRequestOptions
	): Promise<ScrappyRequestNormalizedOptions> {
		if (this.options.defaults) {
			options = await this.mergeRequestOptions(this.options.defaults, options);
		}
		/// Process the url
		let url: any = options.url;
		if (typeof url === 'string') {
			url = new URL(url);
		}
		if (options.query) {
			const search = new URLSearchParams(options.query);
			search.forEach((value, name) => {
				url.searchParams.set(name, value);
			});
		}

		let agentOptions: AgentOptions = {};
		if (options.https) {
			if (options.https.pfx) {
				agentOptions.pfx = options.https.pfx;
				agentOptions.passphrase = options.https.passphrase;
			} else if (options.https.certificate || options.https.privateKey) {
				agentOptions.cert = options.https.certificate;
				agentOptions.key = options.https.privateKey;
				agentOptions.passphrase = options.https.passphrase;
			}
			if (options.https.agentOptions) {
				agentOptions = { ...agentOptions, ...options.https.agentOptions };
			}
		}

		let headers: ScrappyHeaders = {};
		if (options.headers) {
			headers = { ...options.headers };
		}

		let body: Buffer | string | null = null;
		if (options.body) {
			const result = ScrappyBody.serialize(options.body);
			if (result) {
				body = result.body;
				if (result.headers) {
					headers = { ...result.headers, ...headers };
				}
			}
		}

		return {
			retry: {
				attempts: 1,
				...options.retry,
			},
			scrapperOptions: {
				url: url.toString(),
				method: options.method || 'get',
				headers: headers,
				body: body,
				agentOptions: agentOptions,
				simple: options.simple !== false,
			},
		};
	}

	/// Normalize the response
	private async normalizeResponseBody(
		response: WebScrapperResponse,
		options: ScrappyRequestOptions
	): Promise<ScrappyResponseBody> {
		const responseType = options.responseType || ScrappyResponseType.TEXT;
		if (responseType === ScrappyResponseType.BUFFER) {
			return { buffer: response.body };
		}

		const responseEncoding = options.responseEncoding || 'utf8';
		const buffer = response.body;
		const text = buffer.toString(responseEncoding);
		if (responseType === ScrappyResponseType.TEXT) {
			return { buffer, text };
		} else if (responseType === ScrappyResponseType.JSON) {
			return { buffer, text, json: JSON.parse(text) };
		} else if (responseType === ScrappyResponseType.HTML) {
			return { buffer, text, $: cheerio.load(text) };
		} else {
			throw new Error('Invalid response type');
		}
	}
	/// Normalize the response
	private async normalizeResponse(
		response: WebScrapperResponse,
		options: ScrappyRequestOptions
	): Promise<ScrappyResponse> {
		return {
			statusCode: response.statusCode,
			headers: response.headers,
			body: await this.normalizeResponseBody(response, options),
		};
	}

	/**
	 * Perform the request
	 * @param options
	 */
	async request(options: ScrappyRequestOptions): Promise<ScrappyResponse> {
		await this.setup();
		const normalizedOptions = await this.normalizeRequestOptions(options);
		let response: ScrappyResponse = null;
		let error: Error = null;

		// Bail the tests if the attempts
		let isBailed = false;
		const bail = (err?: Error) => {
			error = err || null;
			isBailed = true;
		};

		const attempts = Math.min(Math.max(normalizedOptions.retry.attempts, 1), 16);
		for (let i = 1; i <= attempts; ++i) {
			// Try to perform the request
			try {
				response = await this.doRequest(normalizedOptions.scrapperOptions, options);
				error = null;
			} catch (err) {
				response = null;
				error = err;
				if (normalizedOptions.retry.onError) {
					await normalizedOptions.retry.onError({ error: err, attempt: i }, bail);
				}
			}

			// Test the request if successful
			if (response && normalizedOptions.retry.test) {
				const isOk = await normalizedOptions.retry.test({ response, attempt: i }, bail);
				if (!isOk) {
					response = null;
				}
			}

			// If the request was successful
			if (!isBailed && response) {
				break;
			}

			// On the next retry
			if (!isBailed && normalizedOptions.retry.onRetry) {
				await normalizedOptions.retry.onRetry({ attempt: i }, bail);
			}

			// If the user has given up
			if (isBailed) {
				response = null;
				break;
			}
		}

		// If there is no response
		if (!response) {
			error = error || new Error('Invalid request');
			throw error;
		}
		return response;
	}

	/**
	 * Perform the request
	 * @param options
	 */
	private async doRequest(
		requestOptions: WebScrapperRequestOptions,
		options: ScrappyRequestOptions
	): Promise<ScrappyResponse> {
		const requestResponse = await this.scrapper!.request(requestOptions);
		return this.normalizeResponse(requestResponse, options);
	}

	/**
	 * Run some steps on the scrappy
	 * @param steps
	 */
	async run<Steps extends ScrappySteps>(steps: Steps): Promise<ScrappyStepsResult<Steps>> {
		const stepRunner = new ScrappyStepRunner(this);
		return stepRunner.run(steps);
	}
}
