import { Scrappy } from '../src/Scrappy';
import { ScrappyResponseType, ScrappyRequestOptions } from '../src/Types';
import { URL } from 'url';
import sinon from 'sinon';

/**
 * Dummy test
 */
describe('Scrappy', () => {
	let scrappy: Scrappy;
	beforeAll(async () => {
		scrappy = new Scrappy();
		await scrappy.setup();
	});
	afterAll(async () => {
		await scrappy.clear();
	});

	it('is instantiable', () => {
		expect(scrappy).toBeInstanceOf(Scrappy);
	});

	describe('#clear', () => {
		it('must be clearable', async () => {
			let response = await scrappy.request({
				method: 'get',
				url: 'http://example.com/',
				responseType: ScrappyResponseType.HTML,
			});
			await scrappy.clear();
			response = await scrappy.request({
				method: 'get',
				url: 'http://www.msftncsi.com/ncsi.txt',
				responseType: ScrappyResponseType.TEXT,
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('text');
			expect(response.body.text).toEqual('Microsoft NCSI');
		});
	});

	describe('#request (basic)', () => {
		beforeAll(async () => {
			await scrappy.clear();
		});
		it('must request simple pages', async () => {
			const response = await scrappy.request({
				method: 'get',
				url: 'http://www.msftncsi.com/ncsi.txt',
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('buffer');
			expect(response.body.buffer).toEqual(Buffer.from('Microsoft NCSI'));
			expect(response.body).toHaveProperty('text');
			expect(response.body.text).toEqual('Microsoft NCSI');
		});

		it('must request simple pages as Buffer', async () => {
			const response = await scrappy.request({
				method: 'get',
				url: 'http://www.msftncsi.com/ncsi.txt',
				responseType: ScrappyResponseType.BUFFER,
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('buffer');
			expect(response.body.buffer).toEqual(Buffer.from('Microsoft NCSI'));
		});

		it('must request simple pages as JSON', async () => {
			const response = await scrappy.request({
				method: 'get',
				url: 'https://postman-echo.com/get?c=30',
				query: {
					a: '10',
					b: '20',
				},
				responseType: ScrappyResponseType.JSON,
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('json');
			expect(response.body.json.args).toEqual({
				a: '10',
				b: '20',
				c: '30',
			});
		});

		it('must request simple pages as HTML', async () => {
			const response = await scrappy.request({
				method: 'get',
				url: 'http://example.com/',
				responseType: ScrappyResponseType.HTML,
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('$');
			expect(response.body.$('h1').text()).toEqual('Example Domain');
		});

		it('must not accept other response types', async () => {
			const callback = () =>
				scrappy.request({
					method: 'get',
					url: 'http://example.com/',
					//@ts-ignore
					responseType: 'invalid',
				});
			await expect(callback()).rejects.toThrow();
		});
	});

	describe('#request (POST)', () => {
		beforeAll(async () => {
			await scrappy.clear();
		});
		it('must post form', async () => {
			const response = await scrappy.request({
				method: 'post',
				url: new URL('https://postman-echo.com/post?c=30'),
				query: {
					a: '10',
					b: '20',
				},
				body: {
					form: { formA: '10', formB: '20' },
				},
				responseType: ScrappyResponseType.JSON,
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('json');
			expect(response.body.json.args).toEqual({
				a: '10',
				b: '20',
				c: '30',
			});
			expect(response.body.json).toHaveProperty('form');
			expect(response.body.json.form).toEqual({ formA: '10', formB: '20' });
		});
		it('must post json', async () => {
			const response = await scrappy.request({
				method: 'post',
				url: new URL('https://postman-echo.com/post?c=30'),
				query: {
					a: '10',
					b: '20',
				},
				body: {
					json: { jsonA: '10', jsonB: '20' },
				},
				responseType: ScrappyResponseType.JSON,
			});
			expect(response).toHaveProperty('body');
			expect(response.body).toHaveProperty('json');
			expect(response.body.json.args).toEqual({
				a: '10',
				b: '20',
				c: '30',
			});
			expect(response.body.json).toHaveProperty('json');
			expect(response.body.json.json).toEqual({ jsonA: '10', jsonB: '20' });
		});
		it('must post raw', async () => {
			const content = 'Test content';
			const response = await scrappy.request({
				method: 'post',
				url: new URL('https://postman-echo.com/post?c=30'),
				query: {
					a: '10',
					b: '20',
				},
				body: {
					raw: content,
				},
				headers: {
					'content-type': 'application/x-www-form-urlencoded',
				},
				responseType: ScrappyResponseType.JSON,
			});
			expect(response.body).toHaveProperty('json');
			expect(response.body.json).toHaveProperty('args');
			expect(response.body.json.args).toEqual({
				a: '10',
				b: '20',
				c: '30',
			});
			expect(response.body.json).toHaveProperty('json');
			expect(response.body.json.json).toEqual({ [content]: '' });
		});
	});

	describe('#request (advanced)', () => {
		beforeAll(async () => {
			await scrappy.clear();
		});
		it('must accept URL', async () => {
			const response = await scrappy.request({
				method: 'get',
				url: new URL('https://postman-echo.com/get?c=30'),
				query: {
					a: '10',
					b: '20',
				},
				responseType: ScrappyResponseType.JSON,
			});
			expect(response.body).toHaveProperty('json');
			expect(response.body.json).toHaveProperty('args');
			expect(response.body.json.args).toEqual({
				a: '10',
				b: '20',
				c: '30',
			});
		});

		it('must allow parallel requests', async () => {
			const requests = [
				testBasicRequest(scrappy),
				testBasicRequest(scrappy),
				testBasicRequest(scrappy),
				testBasicRequest(scrappy),
				testBasicRequest(scrappy),
			];
			await Promise.all(requests);
		});

		it('must allow retries', async () => {
			const retryTest = sinon.spy(async ({ response, attempt }, bail) => {
				if (attempt <= 4) {
					return false;
				}
				return true;
			});
			await testBasicRequest(scrappy, {
				retry: {
					attempts: 5,
					test: retryTest,
				},
			});
			expect(retryTest.callCount).toEqual(5);
		});
	});

	describe('#run (basic)', () => {
		beforeAll(async () => {
			await scrappy.clear();
		});
		it('must run a few steps', async () => {
			const response = await scrappy.run({
				ncsi: {
					order: -100,
					async run(scrappy: Scrappy) {
						const response = await scrappy.request({
							method: 'get',
							url: 'http://www.msftncsi.com/ncsi.txt',
							responseType: ScrappyResponseType.TEXT,
						});
						return { text: response.body.text };
					},
				},
				echo: {
					async run(scrappy: Scrappy) {
						const response = await scrappy.request({
							method: 'get',
							url: 'https://postman-echo.com/get?c=30',
							query: {
								a: '10',
								b: '20',
							},
							responseType: ScrappyResponseType.JSON,
						});
						return { json: response.body.json };
					},
				},
			});
			expect(response.ncsi).toHaveProperty('text', 'Microsoft NCSI');
			expect(response.echo).toHaveProperty('json');
			expect(response.echo.json).toHaveProperty('args', { a: '10', b: '20', c: '30' });
		});
	});
});

async function testBasicRequest(scrappy: Scrappy, extraOptions?: Partial<ScrappyRequestOptions>) {
	extraOptions = extraOptions || {};
	const callback = () =>
		scrappy.request({
			method: 'get',
			url: 'http://www.msftncsi.com/ncsi.txt',
			...extraOptions,
		});
	const response = await callback();
	expect(response).toHaveProperty('body');
	expect(response.body).toHaveProperty('buffer');
	expect(response.body.buffer).toEqual(Buffer.from('Microsoft NCSI'));
	return response;
}
